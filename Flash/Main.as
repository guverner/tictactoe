﻿package
{
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	
	public class Main extends MovieClip
	{
		private static var m_instance:Main;
		public static var m_menuStage:MenuStage;
		public static var m_gameStage:GameStage;
		public static var m_endRoundStage:EndRoundStage;
		public var m_timerForComputer:Timer;
		public var m_timerForRemoveEndRoundStage:Timer;
		
		public static function get instance():Main
		{
			return m_instance;
		}
		
		private function displayMenuStage():void
		{
			if (m_menuStage == null)
			{
				m_menuStage = new MenuStage();
			}
			else
			{
				//update "menu stage"
			}
			
			if (m_gameStage && contains(m_gameStage))
			{
				removeChild(m_gameStage);
			}
			
			if (m_endRoundStage && contains(m_endRoundStage))
			{
				removeChild(m_endRoundStage);
			}
			
			addChild(m_menuStage);
		}
		
		public function initMenuStage():void
		{
			displayMenuStage();
		}
		
		private function displayGameStage():void
		{
			if (m_gameStage == null)
			{
				m_gameStage = new GameStage();
			}
			
			if (m_menuStage && contains(m_menuStage))
			{
				removeChild(m_menuStage);
			}
			
			if (m_endRoundStage && contains(m_endRoundStage))
			{
				removeChild(m_endRoundStage);
			}
			
			addChild(m_gameStage);
		}
		
		private function reinitEverything():void
		{
			if (GameEngine.instance.m_whoGoesFirstStep == GameEngine.X)
			{
				GameEngine.instance.m_whoGoes = GameEngine.X;
			}
			else if (GameEngine.instance.m_whoGoesFirstStep == GameEngine.O)
			{
				GameEngine.instance.m_whoGoes = GameEngine.O;
			}
			
			GameEngine.instance.m_xScore = 0;
			GameEngine.instance.m_oScore = 0;
			GameEngine.instance.m_step = 0;
			GameEngine.instance.m_isFinished = false;
			GameEngine.instance.m_isDraw = false;
			
			m_gameStage.initAllParts();
		}
		
		public function initGameStage():void
		{
			displayGameStage();
			
			GameEngine.instance.m_whoGoesFirstStep = GameEngine.WHO_GOES_FIRST;
			reinitEverything();
		}
		
		public function initEndRoundStage():void
		{
			if (m_endRoundStage == null)
			{
				m_endRoundStage = new EndRoundStage();
			}
			
			if (m_menuStage && contains(m_menuStage))
			{
				removeChild(m_menuStage);
			}
			
			//if (m_gameStage && contains(m_gameStage))
			//{
			//removeChild(m_gameStage);
			//}
			
			addChild(m_endRoundStage);
			
			changeWhoWinLabel();
			
			if (GameEngine.instance.m_whoGoesFirstStep == GameEngine.X)
			{
				GameEngine.instance.m_whoGoes = GameEngine.O;
				GameEngine.instance.m_whoGoesFirstStep = GameEngine.O;
			}
			else
			{
				GameEngine.instance.m_whoGoes = GameEngine.X;
				GameEngine.instance.m_whoGoesFirstStep = GameEngine.X;
			}
			
			GameEngine.instance.m_isFinished = false;
			GameEngine.instance.m_step = 0;
			GameEngine.instance.m_isDraw = false;
			
			m_timerForRemoveEndRoundStage = new Timer(1000, 1);
			m_timerForRemoveEndRoundStage.addEventListener(TimerEvent.TIMER_COMPLETE, onTimerCompleteNextRound);
			m_timerForRemoveEndRoundStage.start();
			
			m_gameStage.initAllParts();
		}
		
		public function Main()
		{
			m_instance = this;
			
			//assign all stages to null
			m_menuStage = null;
			m_gameStage = null;
			m_endRoundStage = null;
			
			//init and add to the scene "menu stage"
			initMenuStage();
		}
		
		public function changeWhoGoesLabel():void
		{
			if (GameEngine.instance.m_whoGoes == GameEngine.X)
			{
				m_gameStage.whoGoesLabel.gotoAndPlay("xGoes");
			}
			else
			{
				m_gameStage.whoGoesLabel.gotoAndPlay("oGoes");
			}
		}
		
		public function updateScoreLabels():void
		{
			m_gameStage.xScore.text = GameEngine.instance.m_xScore.toString();
			m_gameStage.oScore.text = GameEngine.instance.m_oScore.toString();
		}
		
		public function changeWhoGoes():void
		{
			if (GameEngine.instance.m_whoGoes == GameEngine.X)
			{
				GameEngine.instance.m_whoGoes = GameEngine.O;
			}
			else
			{
				GameEngine.instance.m_whoGoes = GameEngine.X;
			}
		}
		
		private static function changeWhoWinLabel():void
		{
			if (GameEngine.instance.m_isDraw)
			{
				m_endRoundStage.winLabel.gotoAndStop("draw");
				GameEngine.instance.m_isDraw = false;
			}
			else if (GameEngine.instance.m_whoGoes == GameEngine.X)
			{
				m_endRoundStage.winLabel.gotoAndStop("xWin");
				GameEngine.instance.m_xScore++;
			}
			else if (GameEngine.instance.m_whoGoes == GameEngine.O)
			{
				m_endRoundStage.winLabel.gotoAndStop("oWin");
				GameEngine.instance.m_oScore++;
			}
		}
		
		public function onTimerCompleteNextRound(event:TimerEvent):void
		{
			removeChild(m_endRoundStage);
			m_timerForRemoveEndRoundStage.removeEventListener(TimerEvent.TIMER_COMPLETE, onTimerCompleteNextRound);
			
			//if on next round computer goes first
			computerGoes();
		}
		
		public function onTimerCompleteForCompStep(event:TimerEvent):void
		{
			m_timerForComputer.removeEventListener(TimerEvent.TIMER_COMPLETE, onTimerCompleteForCompStep);
			
			computerGoes();
		}
		
		public function computerPlaceSymbol():void
		{
			var cells:Array = new Array(9);
			var indRow:int;
			var indColumn:int;
			var ind:int = 0;
			for (indRow = 0; indRow < 3; indRow++)
			{
				for (indColumn = 0; indColumn < 3; indColumn++)
				{
					cells[ind] = Cell(GameEngine.instance.m_board[indRow][indColumn]).m_simbol;
					ind++;
				}
			}
			
			if (cells[0] == GameEngine.X && cells[1] == GameEngine.X && cells[2] == GameEngine.NONE)
			{
				Cell(GameEngine.instance.m_board[0][2]).m_simbol = GameEngine.instance.m_whoGoes;
				Cell(GameEngine.instance.m_board[0][2]).changeImage(Cell.NORMAL);
			}
			else if (cells[0] == GameEngine.X && cells[2] == GameEngine.X && cells[1] == GameEngine.NONE)
			{
				Cell(GameEngine.instance.m_board[0][1]).m_simbol = GameEngine.instance.m_whoGoes;
				Cell(GameEngine.instance.m_board[0][1]).changeImage(Cell.NORMAL);
			}
			else if (cells[1] == GameEngine.X && cells[2] == GameEngine.X && cells[0] == GameEngine.NONE)
			{
				Cell(GameEngine.instance.m_board[0][0]).m_simbol = GameEngine.instance.m_whoGoes;
				Cell(GameEngine.instance.m_board[0][0]).changeImage(Cell.NORMAL);
			}
			else if (cells[3] == GameEngine.X && cells[4] == GameEngine.X && cells[5] == GameEngine.NONE)
			{
				Cell(GameEngine.instance.m_board[1][2]).m_simbol = GameEngine.instance.m_whoGoes;
				Cell(GameEngine.instance.m_board[1][2]).changeImage(Cell.NORMAL);
			}
			else if (cells[3] == GameEngine.X && cells[5] == GameEngine.X && cells[4] == GameEngine.NONE)
			{
				Cell(GameEngine.instance.m_board[1][1]).m_simbol = GameEngine.instance.m_whoGoes;
				Cell(GameEngine.instance.m_board[1][1]).changeImage(Cell.NORMAL);
			}
			else if (cells[4] == GameEngine.X && cells[5] == GameEngine.X && cells[3] == GameEngine.NONE)
			{
				Cell(GameEngine.instance.m_board[1][0]).m_simbol = GameEngine.instance.m_whoGoes;
				Cell(GameEngine.instance.m_board[1][0]).changeImage(Cell.NORMAL);
			}
			else if (cells[6] == GameEngine.X && cells[7] == GameEngine.X && cells[8] == GameEngine.NONE)
			{
				Cell(GameEngine.instance.m_board[2][2]).m_simbol = GameEngine.instance.m_whoGoes;
				Cell(GameEngine.instance.m_board[2][2]).changeImage(Cell.NORMAL);
			}
			else if (cells[6] == GameEngine.X && cells[8] == GameEngine.X && cells[7] == GameEngine.NONE)
			{
				Cell(GameEngine.instance.m_board[2][1]).m_simbol = GameEngine.instance.m_whoGoes;
				Cell(GameEngine.instance.m_board[2][1]).changeImage(Cell.NORMAL);
			}
			else if (cells[7] == GameEngine.X && cells[8] == GameEngine.X && cells[6] == GameEngine.NONE)
			{
				Cell(GameEngine.instance.m_board[2][0]).m_simbol = GameEngine.instance.m_whoGoes;
				Cell(GameEngine.instance.m_board[2][0]).changeImage(Cell.NORMAL);
			}
			else if (cells[0] == GameEngine.X && cells[3] == GameEngine.X && cells[6] == GameEngine.NONE)
			{
				Cell(GameEngine.instance.m_board[2][0]).m_simbol = GameEngine.instance.m_whoGoes;
				Cell(GameEngine.instance.m_board[2][0]).changeImage(Cell.NORMAL);
			}
			else if (cells[0] == GameEngine.X && cells[6] == GameEngine.X && cells[3] == GameEngine.NONE)
			{
				Cell(GameEngine.instance.m_board[1][0]).m_simbol = GameEngine.instance.m_whoGoes;
				Cell(GameEngine.instance.m_board[1][0]).changeImage(Cell.NORMAL);
			}
			else if (cells[3] == GameEngine.X && cells[6] == GameEngine.X && cells[0] == GameEngine.NONE)
			{
				Cell(GameEngine.instance.m_board[0][0]).m_simbol = GameEngine.instance.m_whoGoes;
				Cell(GameEngine.instance.m_board[0][0]).changeImage(Cell.NORMAL);
			}
			else if (cells[1] == GameEngine.X && cells[4] == GameEngine.X && cells[7] == GameEngine.NONE)
			{
				Cell(GameEngine.instance.m_board[2][1]).m_simbol = GameEngine.instance.m_whoGoes;
				Cell(GameEngine.instance.m_board[2][1]).changeImage(Cell.NORMAL);
			}
			else if (cells[1] == GameEngine.X && cells[7] == GameEngine.X && cells[4] == GameEngine.NONE)
			{
				Cell(GameEngine.instance.m_board[1][1]).m_simbol = GameEngine.instance.m_whoGoes;
				Cell(GameEngine.instance.m_board[1][1]).changeImage(Cell.NORMAL);
			}
			else if (cells[4] == GameEngine.X && cells[7] == GameEngine.X && cells[1] == GameEngine.NONE)
			{
				Cell(GameEngine.instance.m_board[0][1]).m_simbol = GameEngine.instance.m_whoGoes;
				Cell(GameEngine.instance.m_board[0][1]).changeImage(Cell.NORMAL);
			}
			else if (cells[2] == GameEngine.X && cells[5] == GameEngine.X && cells[8] == GameEngine.NONE)
			{
				Cell(GameEngine.instance.m_board[2][2]).m_simbol = GameEngine.instance.m_whoGoes;
				Cell(GameEngine.instance.m_board[2][2]).changeImage(Cell.NORMAL);
			}
			else if (cells[2] == GameEngine.X && cells[8] == GameEngine.X && cells[5] == GameEngine.NONE)
			{
				Cell(GameEngine.instance.m_board[1][2]).m_simbol = GameEngine.instance.m_whoGoes;
				Cell(GameEngine.instance.m_board[1][2]).changeImage(Cell.NORMAL);
			}
			else if (cells[5] == GameEngine.X && cells[8] == GameEngine.X && cells[2] == GameEngine.NONE)
			{
				Cell(GameEngine.instance.m_board[0][2]).m_simbol = GameEngine.instance.m_whoGoes;
				Cell(GameEngine.instance.m_board[0][2]).changeImage(Cell.NORMAL);
			}
			else if (cells[0] == GameEngine.X && cells[4] == GameEngine.X && cells[8] == GameEngine.NONE)
			{
				Cell(GameEngine.instance.m_board[2][2]).m_simbol = GameEngine.instance.m_whoGoes;
				Cell(GameEngine.instance.m_board[2][2]).changeImage(Cell.NORMAL);
			}
			else if (cells[0] == GameEngine.X && cells[8] == GameEngine.X && cells[4] == GameEngine.NONE)
			{
				Cell(GameEngine.instance.m_board[1][1]).m_simbol = GameEngine.instance.m_whoGoes;
				Cell(GameEngine.instance.m_board[1][1]).changeImage(Cell.NORMAL);
			}
			else if (cells[4] == GameEngine.X && cells[8] == GameEngine.X && cells[0] == GameEngine.NONE)
			{
				Cell(GameEngine.instance.m_board[0][0]).m_simbol = GameEngine.instance.m_whoGoes;
				Cell(GameEngine.instance.m_board[0][0]).changeImage(Cell.NORMAL);
			}
			else if (cells[2] == GameEngine.X && cells[4] == GameEngine.X && cells[6] == GameEngine.NONE)
			{
				Cell(GameEngine.instance.m_board[2][0]).m_simbol = GameEngine.instance.m_whoGoes;
				Cell(GameEngine.instance.m_board[2][0]).changeImage(Cell.NORMAL);
			}
			else if (cells[2] == GameEngine.X && cells[6] == GameEngine.X && cells[4] == GameEngine.NONE)
			{
				Cell(GameEngine.instance.m_board[1][1]).m_simbol = GameEngine.instance.m_whoGoes;
				Cell(GameEngine.instance.m_board[1][1]).changeImage(Cell.NORMAL);
			}
			else if (cells[4] == GameEngine.X && cells[6] == GameEngine.X && cells[2] == GameEngine.NONE)
			{
				Cell(GameEngine.instance.m_board[0][2]).m_simbol = GameEngine.instance.m_whoGoes;
				Cell(GameEngine.instance.m_board[0][2]).changeImage(Cell.NORMAL);
			}
			else
			{
				var randRow:int = 0;
				var randColumn:int = 0;
				while (true)
				{
					randRow = Math.floor(Math.random() * 3);
					randColumn = Math.floor(Math.random() * 3);
					
					if (Cell(GameEngine.instance.m_board[randRow][randColumn]).m_simbol == GameEngine.NONE)
					{
						Cell(GameEngine.instance.m_board[randRow][randColumn]).m_simbol = GameEngine.instance.m_whoGoes;
						Cell(GameEngine.instance.m_board[randRow][randColumn]).changeImage(Cell.NORMAL);
						break;
					}
				}
			}
		
			//GameEngine.instance.printTable();
		}
		
		public function computerGoes():void
		{
			if (GameEngine.instance.m_gameType == GameEngine.VS_COMPUTER && GameEngine.instance.m_whoGoes == GameEngine.O)
			{
				//place simbol to random place and change image of this cell
				GameEngine.instance.m_step++;
				
				computerPlaceSymbol();
				
				GameEngine.instance.checkForFinish();
				
				if (GameEngine.instance.m_isFinished)
				{
					initEndRoundStage();
				}
				else
				{
					changeWhoGoes();
					changeWhoGoesLabel();
				}
			}
		}
	}
}