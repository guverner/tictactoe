﻿package
{
	import flash.display.MovieClip;
	
	public class GameStage extends MovieClip
	{
		public function GameStage()
		{
			toMenuButton.setLabel("MENU");
			toMenuButton.setGoToStageFunction(goToMenuStage);
		}
		
		private function goToMenuStage():void
		{
			Main.instance.initMenuStage();
		}
		
		public function initAllParts():void
		{
			GameEngine.instance.initBoard();
			Main.instance.updateScoreLabels();
			Main.instance.changeWhoGoesLabel();
		}
	}
}
