﻿package
{
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	
	public class CButton extends MovieClip
	{
		private var m_isClicked:Boolean;
		private var m_buttonLabel:String;
		private var m_goToStageFunction:Function;
		
		public function CButton()
		{
			m_buttonLabel = "text";
			buttonMode = true;
			m_isClicked = false;
			mouseChildren = false;
			gotoAndStop("normal");
			addEventListener(MouseEvent.MOUSE_DOWN, onDown);
			addEventListener(MouseEvent.MOUSE_OVER, onOver);
			addEventListener(MouseEvent.MOUSE_OUT, onOut);
			addEventListener(MouseEvent.MOUSE_UP, onUp);
		}
		
		public function setGoToStageFunction(func:Function):void
		{
			m_goToStageFunction = func;
		}
		
		public function setLabel(str:String):void
		{
			m_buttonLabel = str;
			updateButtonlabel();
		}
		
		private function updateButtonlabel():void
		{
			txt.text = m_buttonLabel;
		}
		
		private function onUp(event:MouseEvent):void
		{
			m_isClicked = false;
			gotoAndStop("over");
			updateButtonlabel();
			
			m_goToStageFunction();
		}
		
		private function onDown(event:MouseEvent):void
		{
			m_isClicked = true;
			gotoAndStop("down");
			updateButtonlabel();
		}
		
		private function onOver(event:MouseEvent):void
		{
			if (m_isClicked)
			{
				gotoAndStop("down");
			}
			else
			{
				gotoAndStop("over");
			}
			
			updateButtonlabel();
		}
		
		private function onOut(event:MouseEvent):void
		{
			gotoAndStop("normal");
			m_isClicked = false;
			updateButtonlabel();
		}
	}
}
