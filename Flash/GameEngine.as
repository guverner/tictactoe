package
{
	import flash.utils.Timer;
	
	public final class GameEngine
	{
		private static var 		m_instance:GameEngine = new GameEngine();
		public static const 	NONE:int = 0;
		public static const 	X:int = 1;
		public static const 	O:int = 2;
		public var	 			m_board:Array;
		public var 				m_xScore:int;
		public var 				m_oScore:int;
		public var 				m_whoGoesFirstStep:int;
		public var 				m_whoGoes:int;
		public var	 			m_isFinished:Boolean;
		public var 				m_gameType:int;
		public static const 	VS_PLAYER:int = 1;
		public static const 	VS_COMPUTER:int = 2;
		public var 				m_step:int;
		private var 			m_timerForRemoveEndRoundStage:Timer;
		private var 			m_timerForComputer:Timer;
		public var	 			m_isDraw:Boolean;
		private var 			m_isGameCanStart:Boolean;
		private var 			m_simbolOfPlayer:int;
		private var 			m_whoCanGo:int;
		public static const 	WHO_GOES_FIRST:int = X;
		
		public function GameEngine()
		{
			if(m_instance)
			{
				throw new Error("Error: GameEngine can only be accessed through GameEngine.getInstance()");
			}
			else
			{
				m_xScore = 0;
				m_oScore = 0;
				m_whoGoesFirstStep = X;
				m_whoGoes = m_whoGoesFirstStep;
				m_isFinished = false;
				m_gameType = 0;
				m_step = 0;
				m_timerForRemoveEndRoundStage = null;
				m_timerForComputer = null;
				m_isDraw = false;
				m_isGameCanStart = false;
				m_simbolOfPlayer = X;
				m_whoCanGo = X;
			}
		}
		
		public static function get instance():GameEngine
		{
			return m_instance;
		}
		
		public function printTable():void
		{
			var indRow:int;
			
			for(indRow = 0; indRow < m_board.length; indRow++)
			{
				trace(Cell(m_board[indRow][0]).m_simbol + " " +
				Cell(m_board[indRow][1]).m_simbol + " " +
				Cell(m_board[indRow][2]).m_simbol);
			}
			
			trace();
		}
		
		public function initBoard():void
		{
			if(m_board == null)
			{
				const NUM_ROWS:int = 3;
				const NUM_COLUMNS:int = 3;
				
				var indRow:int;
				var indColumn:int;
				
				if(m_board == null)
				{
					m_board = new Array(NUM_ROWS);
					
					for(indRow = 0; indRow < m_board.length; indRow++)
					{
						m_board[indRow] = new Array(NUM_COLUMNS);
					}
				
					const EXTRA_X:int = 160;
					const EXTRA_Y:int = 115;
					const DIST_BETWEEN_X:int = 5;
					const DIST_BETWEEN_Y:int = 5;
				
					for(indRow = 0; indRow < m_board.length; indRow++)
					{
						for(indColumn = 0; indColumn < m_board[indRow].length; indColumn++)
						{
							var cell:Cell = new Cell(indRow, indColumn);
							cell.x = indColumn * (cell.width + DIST_BETWEEN_X) + EXTRA_X;
							cell.y = indRow * (cell.height + DIST_BETWEEN_Y) + EXTRA_Y;
							cell.m_simbol = NONE;
							Main.m_gameStage.addChild(cell);
							m_board[indRow][indColumn] = cell;
						}
					}
				}
				else
				{
					for(indRow = 0; indRow < m_board.length; indRow++)
					{
						for(indColumn = 0; indColumn < m_board[indRow].length; indColumn++)
						{
							Cell(m_board[indRow][indColumn]).m_simbol;
						}
					}
				}
			}
			else
			{
				cleanBoard();
			}
		}
		
		private function cleanBoard():void
		{
			if(m_board != null)
			{
				var indRow:int;
				var indColumn:int;
				
				for(indRow = 0; indRow < m_board.length; indRow++)
				{
					for(indColumn = 0; indColumn < m_board[indRow].length; indColumn++)
					{
						Cell(m_board[indRow][indColumn]).m_simbol = NONE;
						Cell(m_board[indRow][indColumn]).changeImage(Cell.NORMAL);
					}
				}
			}
		}
		
		public function checkForFinish():void
		{
			if((Cell(m_board[0][0]).m_simbol == m_whoGoes &&
			Cell(m_board[0][1]).m_simbol == m_whoGoes &&
			Cell(m_board[0][2]).m_simbol == m_whoGoes) ||
			(Cell(m_board[1][0]).m_simbol == m_whoGoes &&
			Cell(m_board[1][1]).m_simbol == m_whoGoes &&
			Cell(m_board[1][2]).m_simbol == m_whoGoes) ||
			(Cell(m_board[2][0]).m_simbol == m_whoGoes &&
			Cell(m_board[2][1]).m_simbol == m_whoGoes &&
			Cell(m_board[2][2]).m_simbol == m_whoGoes) ||
			(Cell(m_board[0][0]).m_simbol == m_whoGoes &&
			Cell(m_board[1][0]).m_simbol == m_whoGoes &&
			Cell(m_board[2][0]).m_simbol == m_whoGoes) ||
			(Cell(m_board[0][1]).m_simbol == m_whoGoes &&
			Cell(m_board[1][1]).m_simbol == m_whoGoes &&
			Cell(m_board[2][1]).m_simbol == m_whoGoes) ||
			(Cell(m_board[0][2]).m_simbol == m_whoGoes &&
			Cell(m_board[1][2]).m_simbol == m_whoGoes &&
			Cell(m_board[2][2]).m_simbol == m_whoGoes) ||
			(Cell(m_board[0][0]).m_simbol == m_whoGoes &&
			Cell(m_board[1][1]).m_simbol == m_whoGoes &&
			Cell(m_board[2][2]).m_simbol == m_whoGoes) ||
			(Cell(m_board[0][2]).m_simbol == m_whoGoes &&
			Cell(m_board[1][1]).m_simbol == m_whoGoes &&
			Cell(m_board[2][0]).m_simbol == m_whoGoes))
			{
				m_isFinished = true;
			}
			else if(m_step == 9)
			{
				m_isFinished = true;
				m_isDraw = true;
			}
		}
	}
}