﻿package
{
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	
	public class MenuStage extends MovieClip
	{
		public function MenuStage()
		{
			//change labels for the buttons
			vsPlayerButton.setLabel("VS PLAYER");
			vsComputerButton.setLabel("VS COMP");
			//assign functions for the buttons
			vsPlayerButton.setGoToStageFunction(goToVsPlayerGameStage);
			vsComputerButton.setGoToStageFunction(goToVsComputerGameStage);
		}
		
		private function goToVsComputerGameStage():void
		{
			Main.instance.initGameStage();
			GameEngine.instance.m_gameType = GameEngine.VS_COMPUTER;
		}
		
		private function goToVsPlayerGameStage():void
		{
			Main.instance.initGameStage();
			GameEngine.instance.m_gameType = GameEngine.VS_PLAYER;
		}
	}
}
